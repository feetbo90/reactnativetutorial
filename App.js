import React from 'react';
import { StyleSheet, View } from 'react-native';
import PlaceInput from './src/components/PlaceInput/PlaceInput';
import PlaceList from './src/components/PlaceList/PlaceList';
import placeImage from './src/assets/beautifull.jpg';
import PlaceDetail from './src/components/PlaceDetail/PlaceDetail';
export default class App extends React.Component {

  state = {
    places: [],
    selectedPlace: null  
  };

  placeAddedHandler = (placeName)=> {
      this.setState(prevState => {
        return{
          places: prevState.places.concat(
            { 
              key: Math.random(), 
              name: placeName,
              image: {
               uri: "https://i.imgur.com/Ww0tzZe.png" 
              } 
            })
        };
      });
  };

  placeSelectedHandler = key => {

    this.setState(prevState => {
      return{
        selectedPlace: prevState.places.find(place => {
          return place.key === key;
        })
      };
    });
};

modalClosedHandler = () => {
  this.setState({
    selectedPlace: null
  });
}

placeDeletedHandler = () => {

  this.setState(prevState => {
    return{
      places: prevState.places.filter(place => {
        return place.key !== prevState.selectedPlace.key;
      }), selectedPlace: null
    };
  });
};


  render() {
    return (
      <View style={styles.container}>
        <PlaceDetail 
          selectedPlace={this.state.selectedPlace}
          onItemDeleted={this.placeDeletedHandler}
          onModalClosed={this.modalClosedHandler}
          />
        <PlaceInput onPlaceAdded= {this.placeAddedHandler}/>
        <PlaceList places= {this.state.places} onItemSelected={this.placeSelectedHandler}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: '#0000',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  inputContainer: {
    width: "100%",
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  placeInput: {
    width: "70%"
  },
  pleceButton: {
    width: "30%"
  }
});
